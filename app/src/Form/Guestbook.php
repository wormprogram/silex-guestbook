<?php

namespace Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class Guestbook extends AbstractType
{
    private $class = 'Entity\Guestbook';

    public function getName()
    {
        return 'guestbook';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('message', 'textarea', array(
                'label' => 'guestbook.message',
                'attr' => array('class' => 'form-control', 'rows' => 4)
            )
        );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->class,
            'intention'  => 'guestbook',
            'validation_groups' => array('Guestbook'),
        ));
    }
}