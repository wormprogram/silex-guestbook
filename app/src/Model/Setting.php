<?php

namespace Model;

use Model\Exception\NotFoundItem;

class Setting extends Base
{
    const PER_PAGE = 10;
    const TITLE = 'Гостевая SoftLine';

    public function getByName($name)
    {
        return $this->app->entityManager()->getRepository('\Entity\Setting')
            ->findOneBy(array('name' => $name));
    }

    public function getPerPage()
    {
        /** @var \Entity\Setting $setting */
        $setting = $this->getByName('per_page');
        return $setting ? $setting->getValue() : self::PER_PAGE;
    }

    public function getTitle()
    {
        /** @var \Entity\Setting $setting */
        $setting = $this->getByName('title');
        return $setting ? $setting->getValue() : self::TITLE;
    }

    public function getList()
    {
        $list = $this->app->entityManager()->getRepository('\Entity\Setting')->findAll();
        return $list;
    }

    public function save($id, $value)
    {
        /** @var \Entity\Setting $setting */
        $setting = $this->app->entityManager()->find('\Entity\Setting', (int) $id);
        if (!$setting) {
            throw new NotFoundItem();
        }

        $setting->setValue($value);
        $this->app->entityManager()->persist($setting);
        $this->app->entityManager()->flush();
    }
}