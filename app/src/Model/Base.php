<?php

namespace Model;


class Base
{
    protected $app;

    public function __construct(\Application $app)
    {
        $this->app = $app;
    }
}