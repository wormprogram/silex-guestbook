<?php

namespace Model\Exception;


class NotFoundItem extends \Exception
{
    protected $message = 'Not found item';
}