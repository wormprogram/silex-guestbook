<?php

namespace Model;

use Symfony\Component\Form\Form as Form;
use Entity\User;

class Guestbook extends Base
{
    public function save(Form $form, User $user)
    {
        /** @var $guestbook \Entity\Guestbook */
        $guestbook = $form->getData();
        $guestbook->setUserId($user->getId());
        $guestbook->setUsername($user->getUsername());
        $this->app->entityManager()->persist($guestbook);
        $this->app->entityManager()->flush();
    }

    public function getListQuery()
    {
        $dql = "
            SELECT g.id, g.message, g.username, g.user_id
            FROM \Entity\Guestbook g ORDER BY g.id DESC
        ";

        return $this->app->entityManager()->createQuery($dql);
    }
}