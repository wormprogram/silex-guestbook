<?php

namespace Controller;

use Silicone\Route;
use Silicone\Controller;
use Model\Setting as SettingModel;
use Model\Exception\NotFoundItem;

class Setting extends Controller
{
    /** @var \Application  */
    protected $app;

    /**
     * @Route("/setting", name="setting")
     */
    public function index()
    {
        $setting = new SettingModel($this->app);
        $settingList = $setting->getList();

        $response =  $this->render('setting/list.twig', array(
            'settingList' => $settingList
        ));

        return $response;
    }

    /**
     * @Route("/setting/edit/{id}", name="setting-edit")
     */
    public function edit($id)
    {
        $setting = $this->app->entityManager()->find('\Entity\Setting', (int) $id);

        if (!$setting) {
            return $this->redirect();
        }

        $response =  $this->render('setting/edit.twig', array(
            'setting' => $setting
        ));

        return $response;
    }

    /**
     * @Route("/setting/save/{id}", name="setting-save")
     */
    public function save($id)
    {
        if (!$this->request->isMethod('POST')) {
            return $this->redirect();
        }

        $value = $this->request->get('setting_value', '');
        if (strlen($value) < 1) {
            $this->app->session()->getFlashBag()->add('error', 'Empty value');
            return $this->app->redirect($this->app->url('setting-edit', array('id' => $id)));
        }

        $settingModel = new SettingModel($this->app);
        try {
            $settingModel->save($id, $value);
        } catch (NotFoundItem $e) {
            $this->app->session()->getFlashBag()->add('error', $e->getMessage());
        }

        return $this->redirect();
    }

    private function redirect()
    {
        return $this->app->redirect($this->app->url('setting'));
    }
}