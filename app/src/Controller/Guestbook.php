<?php

namespace Controller;

use Silicone\Route;
use Silicone\Controller;
use Form\Guestbook as GuestbookForm;

use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\Form\Form as Form;
use Model\Setting as SettingModel;
use Model\Guestbook as GuestbookModel;


class Guestbook extends Controller
{
    /** @var \Application  */
    protected $app;

    /**
     * @Route("/guestbook/page/{currentPage}", name="guestbook")
     */
    public function index($currentPage = 1)
    {
        if ($currentPage === 1) {
            return $this->app->redirect('/guestbook/page/1', 301);
        }

        $paginator = $this->getPaginator();

        $totalItems = count($paginator);
        $perPage = $this->getPerPage();
        $pagesCount = ceil($totalItems / $perPage);

        if ($currentPage > $pagesCount) {
            return $this->redirect();
        }

        $paginator->getQuery()
            ->setFirstResult($perPage * ($currentPage-1))
            ->setMaxResults($perPage);

        /** @var \Kilte\Pagination\Pagination $pagination */
        $pagination = $this->app['pagination']($pagesCount, $currentPage);

        $form = $this->app->formType(new GuestbookForm());

        $response =  $this->render('guestbook.twig', array(
            'form' => $form->createView(),
            'messageList' => $paginator,
            'pages' => $pagination->build(),
            'current' => $pagination->currentPage()
        ));

        return $response;
    }

    private function getPaginator()
    {
        $guestbookModel = new GuestbookModel($this->app);
        $query = $guestbookModel->getListQuery();

        $paginator = new Paginator($query, $fetchJoinCollection = true);
        $paginator->setUseOutputWalkers(false);
        return $paginator;
    }

    private function getPerPage()
    {
        $setting = new SettingModel($this->app);
        return $setting->getPerPage();
    }

    /**
     * @Route("/guestbook", name="guestbook-redirect")
     */
    public function redirect()
    {
        return $this->app->redirect($this->app->url('guestbook'), 301);
    }

    /**
     * @Route("/guestbook/save", name="guestbook-save")
     */
    public function save()
    {
        /** @var \Entity\User $user */
        $user = $this->app->user();

        if ($this->request->isMethod('POST') && $user) {
            $form = $this->app->formType(new GuestbookForm());
            $form->submit($this->request);

            if ($form->isValid()) {
                $guestbook = new GuestbookModel($this->app);
                $guestbook->save($form, $user);
            } else {
                $this->app->session()->getFlashBag()->add('error', 'Number of characters should be > 3 and < 3000');
            }
        }

        return $this->app->redirect($this->app->url('guestbook'));
    }

    /**
     * @Route("/guestbook/delete/{messageId}", name="guestbook-delete")
     */
    public function delete($messageId)
    {
        if (!$this->request->isMethod('POST')) {
            return $this->redirect();
        }

        /** @var \Entity\User $user */
        $user = $this->app->user();
        if (!$user) {
            return $this->redirect();
        }

        /** @var \Entity\Guestbook $message */
        $message = $this->app->entityManager()->getRepository('\Entity\Guestbook')
            ->find((int) $messageId);

        if ($message && $message->getUserId() == $user->getId()) {
            $this->app->entityManager()->remove($message);
            $this->app->entityManager()->flush();
        }

        return $this->redirect();
    }

}