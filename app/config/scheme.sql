CREATE TABLE IF NOT EXISTS `Guestbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `message` longtext COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

INSERT INTO `Guestbook` (`id`, `user_id`, `message`, `username`) VALUES
(1, 1, 'hello', '123'),
(2, 1, 'reafe ef erf  кпукпупып', '123'),
(3, 2, '3 сообщение', 'admin'),
(5, 1, '345345 345 345 345 35', '123'),
(7, 1, 'QWETTTYYY', '123'),
(9, 1, 'привет, андрей', '123'),
(10, 2, 'fw ef wef wef waef wef wef wef', 'admin'),
(11, 2, 'You''ve then to make your public assets be accessible through your web/ directory. The best way to do it is to create symbolic links targeting your bundle public assets, you''ve then to execute\r\n\r\nassets:install web/ --symlink\r\n\r\nAnother useful approach when you want to thoroughly customize specific form field rendering block (Twig) is to define a new form theme, here''s the documentati', 'admin'),
(12, 3, 'мой комментарий о', 'александр'),
(14, 3, 'йцйцвйцвйв', 'александр');

CREATE TABLE IF NOT EXISTS `Setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name_idx` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

INSERT INTO `Setting` (`id`, `name`, `value`) VALUES
(1, 'per_page', '6'),
(2, 'title', 'Гостевая книга SoftLine (тест)');

CREATE TABLE IF NOT EXISTS `User` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

INSERT INTO `User` (`id`, `username`, `password`, `email`, `salt`, `roles`) VALUES
(1, '123', '8ruEIXNzcyCjEyHWlrEfZJC9cOFdtf3V3uKiaeqv16S8rdN3Cq1k5866tjtPX5UCg/uqJKUhEZKNSMKxN5ItPg==', '123@123', '53a6cd2ecc1152.66690158', 'a:1:{i:0;s:9:"ROLE_USER";}'),
(2, 'admin', '6EmuQ9I4I7SEi6NcYMLo7hG2//Ou2POay8ySe5UcKehs1XPCjoIHefUx/oJTGtZm0deTSM5rW0O3I3xjHkZrSg==', 'admin@admin', '53a6f14b21d7b4.31656153', 'a:1:{i:0;s:9:"ROLE_USER";}'),
(3, 'александр', 'UdpHjDZv+A/rM+S7vyCjCgucLEYzLsX+3MRXUFvwuRDCXRFREDpPbi0SXW86uDoPSqD8ZzB0XvRYW/2oOUwviw==', '123@123.ru', '53a79d41d3cc55.92170189', 'a:1:{i:0;s:9:"ROLE_USER";}');