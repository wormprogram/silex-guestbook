<?php
/** @var $app Application */

$app['debug'] = true;

require_once __DIR__ . '/config.php';



// WebProfiler
$app->register(new Silex\Provider\WebProfilerServiceProvider(), array(
    'profiler.cache_dir' => $app->getCacheDir() . '/profiler',
    'profiler.mount_prefix' => '/_profiler',
));
$app->register(new Silicone\Provider\WebProfilerServiceProvider());

