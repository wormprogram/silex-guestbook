Гостевая книга
=================

Установка
-------

```
git clone git@bitbucket.org:wormprogram/silex-guestbook.git /your/app/path
cd /your/app/path
curl -s http://getcomposer.org/installer | php
php composer.phar install
```

Дать права на запись для "app/open/".

Сделать исполняемым "app/console".

Для базы данных: создание базы и таблиц.
```
app/console database:create
app/console schema:create
```

Поддерживает 2 окружения: development (+ profiler, logs), production.

Сделано на основе Silicone (сборка Silex).